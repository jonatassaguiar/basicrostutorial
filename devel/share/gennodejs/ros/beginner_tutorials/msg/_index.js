
"use strict";

let Num = require('./Num.js');
let Array = require('./Array.js');

module.exports = {
  Num: Num,
  Array: Array,
};
