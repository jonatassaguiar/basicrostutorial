;; Auto-generated. Do not edit!


(when (boundp 'beginner_tutorials::Array)
  (if (not (find-package "BEGINNER_TUTORIALS"))
    (make-package "BEGINNER_TUTORIALS"))
  (shadow 'Array (find-package "BEGINNER_TUTORIALS")))
(unless (find-package "BEGINNER_TUTORIALS::ARRAY")
  (make-package "BEGINNER_TUTORIALS::ARRAY"))

(in-package "ROS")
;;//! \htmlinclude Array.msg.html


(defclass beginner_tutorials::Array
  :super ros::object
  :slots (_pos _id ))

(defmethod beginner_tutorials::Array
  (:init
   (&key
    ((:pos __pos) 0)
    ((:id __id) 0)
    )
   (send-super :init)
   (setq _pos (round __pos))
   (setq _id (round __id))
   self)
  (:pos
   (&optional __pos)
   (if __pos (setq _pos __pos)) _pos)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:serialization-length
   ()
   (+
    ;; int64 _pos
    8
    ;; int64 _id
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _pos
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _pos (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _pos) (= (length (_pos . bv)) 2)) ;; bignum
              (write-long (ash (elt (_pos . bv) 0) 0) s)
              (write-long (ash (elt (_pos . bv) 1) -1) s))
             ((and (class _pos) (= (length (_pos . bv)) 1)) ;; big1
              (write-long (elt (_pos . bv) 0) s)
              (write-long (if (>= _pos 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _pos s)(write-long (if (>= _pos 0) 0 #xffffffff) s)))
     ;; int64 _id
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _id (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _id) (= (length (_id . bv)) 2)) ;; bignum
              (write-long (ash (elt (_id . bv) 0) 0) s)
              (write-long (ash (elt (_id . bv) 1) -1) s))
             ((and (class _id) (= (length (_id . bv)) 1)) ;; big1
              (write-long (elt (_id . bv) 0) s)
              (write-long (if (>= _id 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _id s)(write-long (if (>= _id 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _pos
#+(or :alpha :irix6 :x86_64)
      (setf _pos (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _pos (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _id
#+(or :alpha :irix6 :x86_64)
      (setf _id (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _id (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get beginner_tutorials::Array :md5sum-) "872f81f8f3384807e2922551bf27f5ce")
(setf (get beginner_tutorials::Array :datatype-) "beginner_tutorials/Array")
(setf (get beginner_tutorials::Array :definition-)
      "int64 pos
int64 id




")



(provide :beginner_tutorials/Array "872f81f8f3384807e2922551bf27f5ce")


