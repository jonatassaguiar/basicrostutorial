#include "ros/ros.h"
#include "beginner_tutorials/Array.h"

#include <sstream>

main(int argc, char **argv)
{
  ros::init(argc, argv, "talker2");

  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<beginner_tutorials::Array>("chatter", 1000);

  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
  {
    beginner_tutorials::Array msg;

    
    for (float theta = 0, alpha = 360; theta<360; theta++, alpha--){

    msg.pos = theta;
    msg.id = alpha ;
    
    ROS_INFO("%d \n %d",msg.id, msg.pos);

    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  }
  return 0;
}